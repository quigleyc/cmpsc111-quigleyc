/* ****************************************************
Carson Quigley
CMPSC 111
19 JAN 2017
Lab 1 
Listing .1 from Lewis and Loftus, slightly modified
Demonstrates the basic structure of a Java application 
******************************************************* */

import java.util.Date;

public class Lab1	{
	public static void main(String[] args)	{
		System.out.println("Carson Q. " + new Date());
		System.out.println("Lab 1");
		//------------------------
		// Prints Words of Wisdom
		//------------------------
		System.out.println("Advice from James Gosline, creator of Java:");
		System.out.println("Don't be intimidates=--give it a try!");
	}	
}


/* output of program

Carson Q. Thu Jan 19 16:01:42 EST 2017
Lab 1
Advice from James Gosline, creator of Java:
Don't be intimidates=--give it a try!

*/
