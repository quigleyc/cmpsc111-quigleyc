/*
Carson Quigley
CMPSC 111
19 JAN 2017
Lab 1 
Listing .1 from Lewis and Loftus, slightly modified
Demonstrates the basic structure of a Java application 
*/

import java.util.Date;

public class Lab1_myown	{
	public static void main(String[] args)	{
		System.out.println("Carson Q. " + new Date());
		System.out.println("Lab 1-myediti\n");

		//I'm saying hello to the world

		System.out.println("Hello World!");
		System.out.println("Welcome to the wonderful world of Java!");
		System.out.println("I can't wait to get into more advanced programs!");
	}	

}


/*

Carson Q. Thu Jan 19 16:01:09 EST 2017
Lab 1-myediti

Hello World!
Welcome to the wonderful world of Java!
I can't wait to get into more advanced programs!


*/
