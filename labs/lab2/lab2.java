/*
The work I am submitting is the result of my own thinking and efforts
Carson Quigley
Lab2 - CMPSC 111 Spring 2017
Date: 01 26 2017
Purpose: To demonstrate my ability to use arithmatic operators and variables in a java program
*/

import java.util.Date;		// Imports tools necessary to access date from OS
import java.util.Scanner;	// Imports tools necessary to use Scanner constructors
public class lab2	{
	
	public static void main(String[] args)	{

		//Printing name, lab number, and Date
		System.out.println("Carson Q.\n Lab 2\n" + new Date() + "\n");
		
		//Declaring variables:
		int product;					//Creates an int variable named "product"
		int input;					//Creates an int variable named 'input"
		int sum;					//Creates an int variable named "sum"
		int quotient;					//Creates an int variable named "quotient"

		Scanner scan = new Scanner(System.in);		//Creates a new scanner object called "scan" for user input

		System.out.println("What number multiplied by 5 results in the product of 30?");
		input = scan.nextInt();				//Reads user input and sets the int "input" to the input
		System.out.println("You said: " + input);

		product = 5 * input;				//Calculates the product of 5 and the user input
		
			if(product == 30)	{		//If the product is correct, asks another question.
				System.out.println("That is correct. Good job");
				System.out.println("Can you also tell me what added to 5 is 30?");
				input = scan.nextInt();
				System.out.println("You said: " + input);
				
				sum = 5 + input;


					if(sum == 30)	{	//If the sum is correct, asks the final question
						System.out.println("That is also correct, Good job.");
						System.out.println("Can you tell me what divided by 10 is 30?");
						input = scan.nextInt();
						System.out.println("You said: " + input);
						
						quotient = input / 10;

						
							if(quotient == 30)	{
								System.out.println("That is correct, good job.");
					}


							else	{	//If the quotient is not correct, asks the user to try again	
								System.out.println("The quotient of " + input + " and 10 is " + quotient );
								System.out.println("Try again");
					}
			}
					
					else	{			//If the sum is incorrect, asks the user to try again
						System.out.println("The sum of " + input + " and 5 is: " + sum);
						System.out.println("Try again.");
			}

		}

		
			else	{					//If the product is incorrect, asks the user to try again
				System.out.println("The product of " + input + " and 5 is: " + product );
				System.out.println("Try again.");
		}
	}
}
