/*
The work I am submitting is the result of my own efforts
Carson Quigley
Lab3
*/

import java.util.Date;
import java.util.Scanner;
import java.text.NumberFormat;

public class lab3	{
	
	public static void main(String[] args)	{

		String name;
		double price;
		int percent;
		double tip;	
		double bill;

		Scanner scan = new Scanner(System.in);
		NumberFormat fmt1 = NumberFormat.getCurrencyInstance();		

		System.out.print("Please enter your name... ");
		name = scan.nextLine();

		System.out.print("Hey there " + name + "! Welcome to the tip calculator!\n");
		System.out.print("Enter the total price of the meal. 	$");
		price = scan.nextDouble();

		System.out.print("Now enter the percentage you would like to tip. (0 - 100)  %");
		percent = scan.nextInt();
		
		tip = (double) percent / 100 * price;
		bill = price + tip;		

		System.out.print("Meal: " + fmt1.format(price) + "\nTip: " + fmt1.format(tip)); 
		System.out.print("\nTotal Bill:  " + fmt1.format(bill) + "\n");
		
	}
}
