//***************************
//Honor Code: The work that I am submitting is a result of my own thinking and efforts.
//Matthew Bissert & Carson Quigley
//CMPSC 111 Spring 2017
//Lab 8
//Date: 02 09 2017
//
//Purpose: Create a sudoku puzzle checker.
//***************************

public class SudokuCheckerMain
{
	public static void main ( String args[] )
	{
		SudokuChecker checker = new SudokuChecker();
		System.out.println("Welcome to the Sudoku Checker!");
				System.out.println("This program checks simple and small 4x4 Sudoku grids for correctness.");
				System.out.println("Each column, row, and 2x2 region contains the numbers 1 through 4 only once.");
				System.out.println("To check your Sudoku, enter your board one row at a time, with each digit separated by a space."); 
				System.out.println("Hit ENTER at the end of a row.");
		
		checker.inputGrid();
		checker.checkGrid();
	}
}
