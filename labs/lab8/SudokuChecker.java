//**************************************************************************************
//Carson Quigley && Matthew Bissert
//All the work being submitted is the culmulative effort of both of us and nobody elses.
//Date: 03/09/17
//**************************************************************************************

import java.util.Scanner;

public class SudokuChecker	{
	private int sum, a1, a2, a3, a4, b1, b2, b3, b4, c1, c2, c3, c4, d1, d2, d3, d4;
	private int reg1, reg2, reg3, reg4, regAll;
 	

	public SudokuChecker()	{
		a1 = 0;	a2 = 0;	a3 = 0;	a4 = 0;
		b1 = 0;	b2 = 0;	b3 = 0;	b4 = 0;
		c1 = 0;	c2 = 0;	c3 = 0;	c4 = 0;
		d1 = 0;	d2 = 0;	d3 = 0;	d4 = 0;
		reg1 = 0; reg2 = 0; reg3 = 0; reg4 = 0;
		sum = 0; regAll = 0; 
	}

	public void inputGrid()	{
		Scanner scan = new Scanner(System.in);
		System.out.print("Type row 1, separated by spaces: ");
		a1 = scan.nextInt();
		a2 = scan.nextInt();
		a3 = scan.nextInt();
		a4 = scan.nextInt();
		System.out.print("Type row 2, separated by spaces: ");
		b1 = scan.nextInt();
		b2 = scan.nextInt();
		b3 = scan.nextInt();
		b4 = scan.nextInt();
		System.out.print("Type row 3, separated by spaces: ");
		c1 = scan.nextInt();
		c2 = scan.nextInt();
		c3 = scan.nextInt();
		c4 = scan.nextInt();
		System.out.print("Type row 4, separated by spaces: ");
		d1 = scan.nextInt();
		d2 = scan.nextInt();
		d3 = scan.nextInt();
		d4 = scan.nextInt();
	}

	public void checkGrid()	{
		sum = a1 + a2 + a3 + a4;
		System.out.print("ROW 1: ");

		if (sum  == 10)	{	
			System.out.print(" GOOD\n");
			regAll++;
		} 
		else {
			System.out.println(" BAD");
		}
		
		sum = b1 + b2 + b3 + b4;
		System.out.print("ROW 2: ");
		
		if (sum  == 10)	{	
			System.out.print(" GOOD\n");
			regAll++;
		} 
		else {
			System.out.println(" BAD");
		}
		
		sum = c1 + c2 + c3 + c4;
		System.out.print("ROW 3: ");
		
		if (sum  == 10)	{	
			System.out.print(" GOOD\n");
			regAll++;
		} 
		else {
			System.out.println(" BAD");
		}
		
		sum = d1 + d2 + d3 + d4;
		System.out.print("ROW 4: ");
			
		if (sum  == 10)	{	
			System.out.print(" GOOD\n");
			regAll++;
		} 
		else {
			System.out.println(" BAD");
		}
		
		sum = a1 + b1 + c1 + d1;
		System.out.print("Column 1: ");
		
		if (sum  == 10)	{	
			System.out.print(" GOOD\n");
			regAll++;
		} 
		else {
			System.out.println(" BAD");
		}
		
		sum = a2 + b2+ c2 + d2;
		System.out.print("Column 2: ");
			
		if (sum  == 10)	{	
			System.out.print(" GOOD\n");
			regAll++;
		} 
		else {
			System.out.println(" BAD");
		}
		
		sum = a3 + b3 + c3 + d3;
		System.out.print("Column 3: ");
			
		if (sum  == 10)	{	
			System.out.print(" GOOD\n");
			regAll++;
		} 
		else {
			System.out.println(" BAD");
		}
		
		sum = a4 + b4 + c4 + d4;
		System.out.print("Column 4: ");
			
		if (sum  == 10)	{	
			System.out.print(" GOOD\n");
			regAll++;
		} 
		else {
			System.out.println(" BAD");
		}
		
		reg1 = a1 + a2 + b1 + b2;
		System.out.print("Region 1: ");
			
		if (reg1  == 10)	{	
			System.out.print(" GOOD\n");
			regAll++;
		} 
		else {
			System.out.println(" BAD");
		}
		
		reg2 = c1 + c2 + d1 + d2;
		System.out.print("Region 2: ");
		
		if (reg2  == 10)	{	
			System.out.print(" GOOD\n");
			regAll++;
		}	
		else {
			System.out.println(" BAD");
		}
		
		reg3 = a3 + a4 + b3 + b4;
		System.out.print("Region 3: ");
		
		if (reg2  == 10)	{	
			System.out.print(" GOOD\n");
			regAll++;
		}	
		else {
			System.out.println(" BAD");
		}
		
		reg4 = c3 + c4 + d3 + d4;
		System.out.print("Region 4: ");
		
		if (reg4  == 10)	{	
			System.out.print(" GOOD\n");
		}	
		else {
			System.out.println(" BAD");
		}
		
		//System.out.println("regAll = " + regAll);	//Use for testing regAll for corretness checking

		if (regAll == 11)	{	
			System.out.println("Sudoku Puzzle is correct!");
		}	
		else {
			System.out.println("Sudoku Puzzle Incorrect.");
		}

	}
}
