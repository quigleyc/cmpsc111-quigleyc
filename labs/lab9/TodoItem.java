public class TodoItem {

  private int id;
  private static int nextId = 0;
  private String priority;
  private String category;
  private String task;
  private boolean done;

  public TodoItem(String p, String c, String t) {
    id = nextId;
    nextId++;
    priority = p;
    category = c;
    task = t;
    done = false;
  }

  public int getId() {		//Reads the ID of the current Item
    return id;
  }

  public String getPriority() {	//Reads the priority of the current item
    return priority;
  }

  public String getCategory() {	//Reads the category of the current item
    return category;
  }

  public String getTask() {	//Reads the task of the current item
    return task;
  }

  public void markDone() {	//Function for marking a task as done
    done = true;
  }

  public boolean isDone() {	//Checks to see if the task is done
    return done;
  }

  public String toString() {	//Changes the file input to a string to be used
    return new String(id + ", " + priority + ", " + category + ", " + task + ", done? " + done);
  }
}
