import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class TodoList {

  private ArrayList<TodoItem> todoItems;
  private static final String TODOFILE = "todo.txt";

  public TodoList() {		//Makes the list an array
    todoItems = new ArrayList<TodoItem>();
  }

  public void addTodoItem(TodoItem todoItem) {
    todoItems.add(todoItem);
  }

  public Iterator getTodoItems() {	//Populates the array
    return todoItems.iterator();
  }

  public void readTodoItemsFromFile() throws IOException {	//Reads the text file
    Scanner fileScanner = new Scanner(new File(TODOFILE));
    while(fileScanner.hasNext()) {
      String todoItemLine = fileScanner.nextLine();
      Scanner todoScanner = new Scanner(todoItemLine);
      todoScanner.useDelimiter(",");
      String priority, category, task;
      priority = todoScanner.next();
      category = todoScanner.next();
      task = todoScanner.next();
      TodoItem todoItem = new TodoItem(priority, category, task);
      todoItems.add(todoItem);
    }
  }

  public void markTaskAsDone(int toMarkId) {	//marks a task as done
    Iterator iterator = todoItems.iterator();
    while(iterator.hasNext()) {
      TodoItem todoItem = (TodoItem)iterator.next();
      if(todoItem.getId() == toMarkId) {
        todoItem.markDone();
      }
    }
  }

  public Iterator findTasksOfPriority(String requestedPriority) {	//Function to sort tasks by priority
    ArrayList<TodoItem> priorityList = new ArrayList<TodoItem>();
    Iterator iterator = todoItems.iterator();
    while (iterator.hasNext())	{
	TodoItem todoItem = (TodoItem)iterator.next();
	if (todoItem.getPriority().equals(requestedPriority))	{
		priorityList.add(todoItem);	
	}
    }
    return priorityList.iterator();
  }

  public Iterator findTasksOfCategory(String requestedCategory) {	//Function to sort tasks by category
    ArrayList<TodoItem> categoryList = new ArrayList<TodoItem>(); 
    Iterator iterator = todoItems.iterator();
    while (iterator.hasNext())	{
	TodoItem todoItem = (TodoItem)iterator.next();
	if (todoItem.getCategory().equals(requestedCategory))	{
		categoryList.add(todoItem);	
	}
    }
    return categoryList.iterator();
  }

  public String toString() {
    StringBuffer buffer = new StringBuffer();
    Iterator iterator = todoItems.iterator();
    while(iterator.hasNext()) {
      buffer.append(iterator.next().toString());
      if(iterator.hasNext()) {
        buffer.append("\n");
      }
    }
    return buffer.toString();
  }

}
