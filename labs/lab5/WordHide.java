//***************************
//Honor Code: The work that I am submitting is a result of my own thinking and efforts.
//Carson Quigley & Matthew Bissert 
//CMPSC 111 Spring 2017
//Lab 5
//Date: 02 16 2017
//
//Purpose: Make it so that nobody can find my word. 
//***************************
import java.util.Random;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List; 
import java.lang.Math;
	
public class WordHide {

	public static void main(String[] args) {
		
		
        	String input;
        	String output;
        	Scanner scan = new Scanner(System.in);
        	
		System.out.println("Enter a word, 10 characters or less: ");
		input = scan.nextLine();		

		output = input.toUpperCase();
				
        	// Generate random string for alphabet filler
        	Random randomgen = new Random();
    		//String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890`-=[];',./\\{}:\"|<>?~!@#$%^&*()_+ ";	//Not used. Changed generator to use user input instead.
		
		String pad = new String(input + input.charAt(randomgen.nextInt(input.length())));
		
		int usrlength = input.length();
		output = input.substring(0,usrlength).toUpperCase();
		System.out.println("You've entered: " + output);

		for (int length = input.length(); length < 10; length++)	{
														
			pad = pad + input.charAt(randomgen.nextInt(input.length()));	//pads user input with random characters
		}											
        	
		output = pad.substring(0,10).toUpperCase();

		// Generate random indices to place the 10-char word...
        	int[] indices = new int[10];	
		for (int outputnum = 0; outputnum < 10; outputnum++){ 		//Keeps track of the number of spaces that still need to be reserved

			indices[outputnum] = (int)(Math.random() * 400);	//Selects 10 random spaces out of 400 to place the users word
        	}
        	        
        	// Sort the indices so that we can insert the user's word
        	Arrays.sort(indices);
        	int outputnum = 0; 	//index for insertion
        	        
        	// Output 400 characters in a 20x20 grid
        	for (int gridspace = 1; gridspace <=400; gridspace++) {
        		
			if (outputnum < 10 && gridspace == indices[outputnum]) {

                        	System.out.print(output.charAt(outputnum));	//fills reserved spaces with the characters from the user input
                        	outputnum++;

			} else {
				
				System.out.print(input.charAt(randomgen.nextInt(input.length())));
                        	//System.out.print("_"); // replace the above line with this one to test where the secret word has been inserted.
                	}
          	
			// New line at the end of 20 characters.
                	if (gridspace % 20 == 0) {

                		System.out.print("\n");

                	} else {

                        	System.out.print(" | ");
            }
        }
    }
}
