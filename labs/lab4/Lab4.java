//=================================================
//All edits made to this file are my own work
//Carson Quigley - 02-09-2017
//Draws the things on the page in the window
//=================================================
import java.awt.*;
import javax.swing.JApplet;

public class Lab4 extends JApplet
{
  //-------------------------------------------------
  // Use Graphics methods to add content to the drawing canvas
  //-------------------------------------------------
  public void paint(Graphics page)	{
	
	Color BOOK_BROWN = new Color(66, 42, 0);

	page.setColor(Color.cyan);		//Make the sky blue!
	page.fillRect(0,0,600,400);

	page.setColor(BOOK_BROWN);
	page.fillRect(240,310,40,80);		//Draw a Tree with a Brown Trunk and Green leaves

	page.setColor(Color.green);		
	page.fillRect(180,270,160,40);		//Bottom rectangle
	page.fillRect(200,240,120,30);		//Second rectangle
	page.fillRect(220,220,80,20);		//Third rectangle
	page.fillRect(240,210,40,10);		//Smallest rectangle

	page.setColor(BOOK_BROWN);		//Draw brown ground
	page.fillRect(0,350,600,20);

	page.setColor(Color.yellow);		//Draw the Big Yellow Sun
	page.fillOval(0,0,80,80);

	page.drawLine(20,80,10,140);		//Draw the Sun Rays
	page.drawLine(80,20,140,10);
	page.drawLine(40,85,40,140);
	page.drawLine(60,80,100,140);
	page.drawLine(85,40,180,40);
	page.drawLine(80,60,120,80);

	page.fillArc(350,250,100,100,60,240); 	//Draw pac-man

	page.fillOval(440,285,30,30);		//Draw pac-dots
	page.fillOval(490,285,30,30);
	page.fillOval(540,285,30,30);
	
	}
}


