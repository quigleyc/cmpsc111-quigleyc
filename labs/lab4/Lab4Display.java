//=============================================
//All changes made to this file are my own work
//Carson Quigley - 02-09-2017
//Creates the window for the artwork program
//=============================================

import javax.swing.*;

public class Lab4Display
{
  public static void main(String[] args)
  {
    JFrame window = new JFrame(" Carson Quigley ");

    // Add the drawing canvas and do necessary things to
    // make the window appear on the screen!
    window.getContentPane().add(new Lab4());
    window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    window.setVisible(true);
    window.setSize(600, 400);
  }
}

