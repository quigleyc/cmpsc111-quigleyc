/* ***********************************************************************************************
 *Carson Quigley & Zijun Zhou
 *HONOR CODE: The work I am submitting is a result of our own collaboratie thinking and efforts
 *CMPSC 111
 *GREGORY KAPFHAMMER
 *Lab 6
 *The purpose of this assignment is to ensure ability to manipulate string with random generators
 *23 February 2017
 * ***********************************************************************************************/

import java.util.Date;
import java.util.Scanner;
import java.util.Random;

public class DNAcompliments		{
	
	public static void main(String[] args)	{	

		Scanner input = new Scanner(System.in);
		String dnastring, compliment, DNA;

		System.out.print("\nEnter a DNA string using only A, T, C, and G: ");
		dnastring = input.nextLine();
		dnastring = dnastring.toUpperCase();
		compliment = dnastring.toUpperCase();
		System.out.println("You've entered: " + dnastring + ".");
		
		compliment = compliment.replace('A', 'Y');	//replaces A's with a placeholder 'Y' to avoid conflict
		compliment = compliment.replace('T', 'A');
		compliment = compliment.replace('Y', 'T');	//replaces placeholder Y's with 'T'

		compliment = compliment.replace('C', 'X');
		compliment = compliment.replace('G', 'C');
		compliment = compliment.replace('X', 'G');

		System.out.println("The compliment of " + dnastring + " is " + compliment + "\n");

		
		char newletter;
		int dnalength, pointA;
		Random randgen = new Random();
		dnalength = dnastring.length();
		pointA = randgen.nextInt(dnalength+1);
		DNA = "ATCG";
		newletter = DNA.charAt(randgen.nextInt(DNA.length()));
			
		System.out.println("Choosing a point:");
		System.out.println("Everything up till the point: " + dnastring.substring(0,pointA));
		System.out.println("Everything after the point: " + dnastring.substring(pointA));
		
		System.out.println("\nInsterting a character: " + dnastring.substring(0,pointA) + newletter + dnastring.substring(pointA));
		System.out.println("Removing a character: " + dnastring.substring(0,pointA-1) + dnastring.substring(pointA));

		newletter = DNA.charAt(randgen.nextInt(DNA.length()));
		System.out.println("Changing a character:" + dnastring.substring(0,pointA-1) + newletter + dnastring.substring(pointA));
	}
}

