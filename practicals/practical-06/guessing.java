import java.io.IOException;
import java.util.Scanner;
import java.util.Date;
import java.util.Random;

public class guessing	{
	public static void main(String[] args) throws IOException{
		int answer, guess, input;
		boolean correct;
		
		guess = 0;
		correct = false;
		Scanner scan = new Scanner(System.in);
		Random gen = new Random();
		answer = gen.nextInt(100);
		//System.out.println(answer);	//Used for debugging

		System.out.println("Welcome to the guessing game!");
		System.out.println("I have picked a number between 1 and 100, try to guess it!\n");

			while (correct != true)	{
			input = scan.nextInt(); 
				
				if (input == answer)	{
					System.out.println("That's right!");
					guess++;
					correct = true;
					break;

				} else {
					if (input >= answer)	{
						System.out.println("Too high!\n");
						guess++;
					}
					if (input <= answer)	{
						System.out.println("Too low!\n");
						guess++;
					}
				}
			}	
		
		System.out.println("Number of guesses: " + guess);
	}
}		
