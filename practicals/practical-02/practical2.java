/*

Carson Quigley
Practical 2, January 26
Prints a picture

*/

import java.util.Date;
public class practical2	{


	public static void main(String[] args)	{

		
		System.out.println("Carson Quigley, CMPSC-111\n" + new Date() + "\n");
	
		System.out.println("     _____________    							    ");
		System.out.println("  __/      --     \\__							    ");
		System.out.println(" /___________________\\                   ________________                      ");
		System.out.println("/|                   |\\                 /                \\______              ");
		System.out.println(" |___________________|     _____________\\______                 \\____          ");
		System.out.println(" |                   |    /                  /    |                  \\_________ ");
		System.out.println("  |                 |  ___\\_________________/     |                             ");
		System.out.println("  |   C O F F E E   | /                    |       |                             ");
		System.out.println("  |                 | \\____________________|        \\________                  ");
		System.out.println("  |                 |   /                  |       /                             ");
		System.out.println("   |_______________|    \\__________________|      |                              ");
		System.out.println("   |               |        /               \\     |               _____________  ");
		System.out.println("   |               |        \\________________\\_____\\_____________/             ");
		System.out.println("   |_______________|");

	}
}

