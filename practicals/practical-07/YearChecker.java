//***************************** CMPSC 111 Practical 8 October 30, 2015
//
// Purpose: determine if user's year input is a leap year, cicada brood II
// emergence year, or a peak sunspot year.
//*****************************
import java.util.Scanner;

public class YearChecker {
    //Create instance variables
    int year;
    //Create a constructor
    public YearChecker(int y) { 
	    year = y;
    }

    // a method that checks if the user's input year is a leap year
    public boolean isLeapYear() { 
    	if (year % 4 != 0)	{
		return false;
	} else if(year % 100 == 0)	{
		return false;
	} else if (year % 400 != 0)	{
		return false;
	} else if (year % 8000 == 0)	{
		return false;
	} else {
		return true;
	}
    }

    // a method that checks if the user's input year is a cicada year
    public boolean isCicadaYear()	{
		if (Math.abs(year-2013) % 17 == 0)	{
			return true;
		} else {
			return false;
		}
	}

    // a method that check if the user's input year is a sunspot year
    public boolean isSunspotYear()	{
		if (Math.abs(year-2013) % 11 == 0)	{
			return true;
		} else {
			return false;
		}
	}
}
