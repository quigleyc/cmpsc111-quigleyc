//*****************************
// CMPSC 111
// Practical 8
// October 30, 2015
//
// Purpose: a program that determines what activities happen
// during a specific year.
//*****************************

import java.util.Date;
import java.util.Scanner;

public class YearCheckerMain
{
    public static void main ( String[] args)
    {
        //Variable dictionary
        Scanner scan = new Scanner(System.in);
        int userInput;

        System.out.print("Please enter a year between 1000 and 10000:	");
        userInput = scan.nextInt();
	while (userInput < 1000 || userInput > 10000)	{
		System.out.print("\nInvalid input\n\nPlease enter a new year:	");
		userInput = scan.nextInt();
	}
        	YearChecker activities = new YearChecker(userInput);
	
        	activities.isLeapYear();
		if (activities.isLeapYear() == true)	{
			System.out.println(userInput + " is a leap year.");
		} else {
			System.out.println(userInput + " is not a leap year.");
		}
		activities.isCicadaYear();
		if (activities.isCicadaYear() == true)	{
			System.out.println(userInput + " is a brood II cicada emergence year.");
		} else {
			System.out.println(userInput + " is not a brood II cicada emergence year.");
		}
		activities.isSunspotYear();
		if (activities.isSunspotYear() == true)	{
			System.out.println(userInput + " is a sunspot year.");
		} else {
			System.out.println(userInput + " is not a sunspot year.");
		}
        System.out.println("Thank you for using this program.");
    }
}
