//=================================================
// Draw two boxes that display a color and the complement of the color.
//=================================================

import java.awt.*;
import javax.swing.JApplet;

public class PaintDrawingCanvas extends JApplet {

  //-------------------------------------------------
  // Use Graphics methods to add content to the drawing canvas
  //-------------------------------------------------
  public void paint(Graphics page) {
        Color userColor = new Color(DisplayDrawingCanvas.redValue,
        DisplayDrawingCanvas.greenValue, DisplayDrawingCanvas.blueValue);

    	page.setColor(userColor);
	page.fillRect(0,0,600,200);

    	Color userComplementaryColor = new Color(255 - DisplayDrawingCanvas.redValue, 255 - DisplayDrawingCanvas.greenValue,
	255 - DisplayDrawingCanvas.blueValue);

    	page.setColor(userComplementaryColor);
	page.fillRect(0,200,600,200);
  }
}
