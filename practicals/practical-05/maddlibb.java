/*
HONOR CODE: All the work sumbitted is the product of my own thinking
Carson Quigley
February 24 2017
Practical 05
The purpose of this assignment is to practice using variable types and strings
*/

import java.util.Scanner;
import java.util.Date;

public class maddlibb	{
	
	public static void main(String[] args)	{
		
		Scanner input = new Scanner(System.in);
		int a, b, sum;
		double c;
		String name, noun1, noun2, adjective, verb, place;
		
		System.out.print("Enter a name: ");
		name = input.nextLine();
		System.out.print("\nEnter a place: ");
		place = input.nextLine();
		System.out.print("\nEnter a singular noun: ");
		noun1 = input.nextLine();
		System.out.print("\nEnter a plural noun: ");
		noun2 = input.nextLine();
		System.out.print("\nEnter an adjective: ");
		adjective = input.nextLine();
		System.out.print("\nEnter a verb: ");
		verb = input.nextLine();
		System.out.print("\nEnter a whole number: ");
		a = input.nextInt();
		System.out.print("\nEnter another whole number: ");
		b = input.nextInt();
		System.out.print("\nEnter any number: ");
		c = input.nextDouble();
		
		sum = a + b;

		System.out.println("\nOne day " + name + " was " + verb + "ing to " + place + "  when suddenly " + a + " " + noun2 + " fell down right on top of them!");
		System.out.println("Luckily " + name + " brought along their " + noun1 + " to block most of the " + noun2 + ".");
	        System.out.println(" But just when " + name + " thought it was over, another " + b + " " + noun2 + " flew at them!");
		System.out.println("\"That's a lot of " + noun2 + "\" " + name + " exclaimed, \"That's " + adjective + "! \"It's almost " + c  + " " + noun2 + "!\"");
		System.out.println("Just after they exalimed this, a passerby walked past and said: \"No, it's " + sum + " " +  noun2 + "!\n");

	}
}
