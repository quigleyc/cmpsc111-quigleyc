
public class variables	{

	public static void main (String[] args)	{
		
		int  num;		//Declare a bunch of variables
		float point;
		char letter;
		boolean yesno;
		String text; 
		final double pi;
		
		num = 2;		//Assign a bunch of values to said variables
		point = 2;
		letter = 'A';
		yesno = false;
		text = "hello world";
		pi = 3.14159;

		System.out.println("int: " + num + "\n" + "float: " + point + "\n" + "char: " + letter + "\n" + "boolean: " + yesno + "\n" + 
			"String: " + text + "\n" + "constant double: " + pi); //print the value of each variable
	}
}
