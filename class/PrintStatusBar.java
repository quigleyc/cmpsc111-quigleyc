

public class PrintStatusBar	{

	public static void main(String[] args) {
	
		System.out.println();
			
		String numberOfSymbols = args[0]; //First argument
		int numSymC = Integer.parseInt(numberOfSymbols);
		String printedSymbol = args[1];	//Second argument
	
		for(int i = 0; i < numSymC; i++)	{
			System.out.print(printedSymbol);
		}
		
		System.out.println("\n");	
	}
}
