import java.util.Random;

public class randomgen	{
	
	public static void main(String[] args)	{
		
		Random generator = new Random();
		int value1;
		float value2;
		
		value1 = generator.nextInt();
		System.out.println("A random integer: " + value1);
		
		value1 = generator.nextInt(10);
		System.out.println("From 0 to 9: " + value1);

		value1 = generator.nextInt(10) + 1;
		System.out.println("From 1 to 10: " + value1);

		value1 = generator.nextInt(15) + 20;
		System.out.println("From 20 to 34: " + value1);

		value2 = generator.nextFloat();
		System.out.println("A random float (between 0-1): " + value2);

		value2 = generator.nextFloat() * 6;	// 0.0 to 5.999999
		value1 = (int)value2 + 1;
		System.out.println("From 1 to 6: " + value1);

	}
}
