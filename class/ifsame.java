//****************
//Carson Quigley
//****************
import java.util.Scanner;

public class ifsame	{
	public static void main(String[] args)	{
		int input1, input2;
		Scanner scan = new Scanner(System.in);

		System.out.print("\nEnter a whole number: ");
		input1 = scan.nextInt();
		System.out.print("\nEnter another whole number: ");
		input2 = scan.nextInt();

		if (input1 == input2)	{
			System.out.println("\nI love you.");
		}
		else	{
			System.out.println("\nThese two numbers are not the same!");
		}
		
		System.out.println("Thank you for your numbers: " + input1 + " and " + input2 + "\n");
	}	
}
